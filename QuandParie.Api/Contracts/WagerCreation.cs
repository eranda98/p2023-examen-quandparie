﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuandParie.Api.Contracts
{
    public record WagerCreation(
        [Required][Range(1, int.MaxValue)] decimal Amount,
        [Required][NotEmptyList] IReadOnlyList<Guid> OddsIds);
}
