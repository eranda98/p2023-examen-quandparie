﻿using QuandParie.Core.ReadOnlyInterfaces;
using System;
using System.Collections.Generic;

namespace QuandParie.Core.Domain
{
    public class Customer : IReadOnlyCustomer
    {
        public Customer(string email, string firstName, string lastName)
        {
            Email = email;
            FirstName = firstName;
            LastName = lastName;
        }

        public string Email { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public bool IsIdentityVerified { get; set; }
        public string Address { get; set; }

        public bool CanWager
            => IsIdentityVerified && Address != null;

        public decimal Balance { get; private set; }

        public void Debit(decimal amount)
        {
            if (amount <= 0)
                throw new ArgumentOutOfRangeException(nameof(amount), amount, $"Amount must be strictly positive");

            if (Balance <= amount)
                throw new ArgumentOutOfRangeException(nameof(amount), amount, $"Balance of {Email} must be equal or higher");

            Balance -= amount;
        }

        public void Credit(decimal amount)
        {
            if (amount <= 0)
                throw new ArgumentOutOfRangeException(nameof(amount), amount, $"Amount must be strictly positive");

            Balance += amount;
        }
    }
}