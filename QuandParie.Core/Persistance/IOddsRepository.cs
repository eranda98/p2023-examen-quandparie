﻿using QuandParie.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuandParie.Core.Persistance
{
    public interface IOddsRepository
    {
        Task SaveAsync(Odds odds);
        Task<Odds> GetAsync(Guid oddsId);
        Task<IReadOnlyList<Odds>> GetForMatchAsync(string match);
    }
}