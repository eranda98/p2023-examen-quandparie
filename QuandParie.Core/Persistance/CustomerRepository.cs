﻿using QuandParie.Core.Domain;
using System;
using System.Threading.Tasks;

namespace QuandParie.Core.Persistance
{
    public interface CustomerRepository
    {
        Task SaveAsync(Customer customer);
        Task<Customer> GetAsync(string email);
    }
}