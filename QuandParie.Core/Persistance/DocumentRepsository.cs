﻿using QuandParie.Core.Domain;
using System;
using System.Threading.Tasks;

namespace QuandParie.Core.Persistance
{
    public interface DocumentRepsository
    {
        Task SaveAsync(DocumentType documentKind, string documentId, byte[] document);
    }
}