﻿namespace QuandParie.Core.ReadOnlyInterfaces
{
    public interface IReadOnlyCustomer
    {
        string Address { get; }
        decimal Balance { get; }
        bool CanWager { get; }
        string Email { get; }
        string FirstName { get; }
        bool IsIdentityVerified { get; }
        string LastName { get; }
    }
}