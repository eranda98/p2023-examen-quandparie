﻿using System;
using System.Collections.Generic;

namespace QuandParie.Core.ReadOnlyInterfaces
{
    public interface IReadOnlyWager
    {
        decimal Amount { get; }
        IReadOnlyCustomer Customer { get; }
        Guid Id { get; }
        bool IsProcessed { get; }
        IReadOnlyList<IReadOnlyOdds> Odds { get; }
        IEnumerable<Guid> OddsIds { get; }
        decimal PotentialPayoff { get; }
    }
}