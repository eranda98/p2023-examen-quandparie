﻿using QuandParie.Core.Domain;

namespace QuandParie.Core.Services
{
    public interface AddressProofer
    {
        bool Validates(Customer customer, out string address, byte[] addressProofer);
    }
}