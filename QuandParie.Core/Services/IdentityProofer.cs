﻿using QuandParie.Core.Domain;

namespace QuandParie.Core.Services
{
    public interface IdentityProofer
    {
        bool Validates(Customer customer, byte[] identityProof);
    }
}