﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using QuandParie.Core.Persistance;
using System.Threading.Tasks;
using QuandParie.Core.ReadOnlyInterfaces;

namespace QuandParie.Core.Application
{
    public class RegistrationApplication
    {

        private readonly CustomerRepository customerRepo;
        private readonly DocumentRepsository documentRepo;
        private readonly IdentityProofer identityProof;
        private readonly AddressProofer addressProof;

        public RegistrationApplication(CustomerRepository customer, DocumentRepsository document, IdentityProofer identity, AddressProofer address)
        {
            this.customerRepo = customer;
            this.documentRepo = document;
            this.identityProof = identity;
            this.addressProof = address;
        }

        public async Task<IReadOnlyCustomer> CreateAccount(string s1, string s2, string s3)
        {
            var o = new Customer(s1, s2, s3);
            await customerRepo.SaveAsync(o);

            return o;
        }

        public async Task<IReadOnlyCustomer> GetAccount(string s)
        {
            return await customerRepo.GetAsync(s);
        }

        public async Task<bool> UploadIdentityProof(string c2, byte[] b)
        {
            var c1 = await customerRepo.GetAsync(c2);
            if (!identityProof.Validates(c1, b))
                return false;

            await documentRepo.SaveAsync(DocumentType.IdentityProof, c2, b);

            c1.IsIdentityVerified = true;
            await customerRepo.SaveAsync(c1);

            return true;
        }

        public async Task<bool> UploadAddressProof(string s, byte[] b)
        {
            var c = await this.customerRepo.GetAsync(s);
            if (!addressProof.Validates(c, out var s2, b))
                return false;

            await documentRepo.SaveAsync(DocumentType.AddressProof, s, b);

            c.Address = s2;
            await this.customerRepo.SaveAsync(c);

            return true;
        }
    }
}
