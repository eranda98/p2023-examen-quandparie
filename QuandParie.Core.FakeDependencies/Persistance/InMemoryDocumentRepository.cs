﻿using QuandParie.Core.Domain;
using QuandParie.Core.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.FakeDependencies.Persistance
{
    public class InMemoryDocumentRepository : DocumentRepsository
    {
        public Dictionary<(DocumentType, string), byte[]> Documents = new();

        public void Save(DocumentType documentKind, string documentId, byte[] document)
            => Documents[(documentKind, documentId)] = document;

        public Task SaveAsync(DocumentType documentKind, string documentId, byte[] document)
        {
            Save(documentKind, documentId, document);
            return Task.CompletedTask;
        }
    }
}
