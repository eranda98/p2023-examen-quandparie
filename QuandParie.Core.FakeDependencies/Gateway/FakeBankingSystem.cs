﻿using QuandParie.Core.Gateway;
using QuandParie.Core.Gateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.FakeDependencies.Gateway
{
    public class FakeBankingSystem : IBankingSystem
    {
        public List<ReceivedPaymentData> Payments = new()
        {
            new("x9216b", 42)
        };
        public List<WireTransferData> Transfers = new();

        public Task<ReceivedPaymentData> Receive(string paymentToken)
            => Task.FromResult(Payments.FirstOrDefault(
                payment => payment.Token == paymentToken));

        public Task Transfer(WireTransferData transfer)
        {
            Transfers.Add(transfer);
            return Task.CompletedTask;
        }
    }
}
